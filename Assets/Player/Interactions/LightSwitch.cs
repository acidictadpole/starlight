using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Automatically will add an interactable component to the
/// gameobject this is added to at runtime.
/// </summary>
public class LightSwitch : MonoBehaviour
{

    public Light targetLight;
    Interactable thisInteraction;

    public string GUILabel = "Light";
    bool SwitchState = false;

    void Awake()
    {
        thisInteraction = gameObject.GetOrAddComponent<Interactable>();
        thisInteraction.guiName = GUILabel;
        thisInteraction.Interaction += OnInteraction;
        //Sets the target light to be on/off depending on initial state
        targetLight.enabled = SwitchState;
    }

    private void OnInteraction(object sender, Interactable.InteractionEventArgs e)
    {
        if (e.interactionType == Interactable.InteractionType.CLICK)
        {
            SwitchState = !SwitchState;
            targetLight.enabled = SwitchState;
        }
    }
}
