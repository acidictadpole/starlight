using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controllable : MonoBehaviour
{
    public enum ControllerType
    {
        Crewmate, //Used as the default.
        Ship,
        Turret
    }

    public static IInputController ControllerFor(Controllable c)
    {
        return c.controllerType switch
        {
            ControllerType.Crewmate => new CrewmateMovementController(c.gameObject.transform),
            ControllerType.Ship => new StarshipInputController(c.gameObject),
            ControllerType.Turret => new TurretInputController(c.transform),
            _ => throw new System.NotImplementedException()
        };
    }

    public ControllerType controllerType;
}
