using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class FlightConsole : MonoBehaviour
{

    Interactable thisInteraction;
    [SerializeField]
    Controllable ship;

    void Awake()
    {
        thisInteraction = gameObject.GetOrAddComponent<Interactable>();
        thisInteraction.guiName = "Flight Console";
        thisInteraction.Interaction += OnInteraction;
        //I didn't know about ??=, but this only sets ship if it's null.
        ship ??= GetComponentInParent<Controllable>();
    }

    private void OnInteraction(object sender, Interactable.InteractionEventArgs e)
    {
        if (e.interactionType == Interactable.InteractionType.CLICK){
            e.interactor.TakeControlOf(ship);
        }
    }
}
