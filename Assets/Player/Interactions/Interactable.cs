﻿using System;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public enum InteractionType { HOVER, CLICK }
    /**
     * The name that will appear when hovering over this Interactable
     */
    [Tooltip("The name that will appear when hovering this.")]
    [SerializeField]
    public string guiName;

    /**
     * Event handler for Interaction events
     */
    public event EventHandler<InteractionEventArgs> Interaction;

    public void Interact(InteractionType interactionType, PlayerController player)
    {
        var args = new InteractionEventArgs(interactionType, player);
        Interaction?.Invoke(this, args);
    }

    /**
     * Information to cover the Interaction
     */
    public class InteractionEventArgs : EventArgs
    {
        public PlayerController interactor;
        public InteractionType interactionType;
        public InteractionEventArgs(InteractionType interactionType, PlayerController interactor)
        {
            this.interactor = interactor;
            this.interactionType = interactionType;
        }
    }
}