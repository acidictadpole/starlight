using UnityEngine;

public class PlayerController : MonoBehaviour
{
    IInputController inputController;
    /// <summary>
    ///The original controllable that this player is controlling.
    ///We hold on to it so that we know what to jump back to when
    ///the player is done controlling whatever else.
    /// </summary>
    public Controllable OriginalControlled;

    /// <summary>
    /// The currently controlled controllable.
    /// </summary>
    public Controllable CurrentlyControlled { get; private set; }

    MouseLook mlook; 

    void Awake()
    {
        mlook = GetComponent<MouseLook>();
        //Start the game and get the appropriate input controller.
        OriginalControlled ??= GetComponentInParent<Controllable>();
        TakeControlOf(OriginalControlled);
        mlook.body = OriginalControlled.transform;
    }

    // Update is called once per frame
    void Update()
    {
        inputController?.HandleInput();

        //Kinda temporary. Just to get back to controlling the crewdude.
        if (Input.GetKeyDown(KeyCode.Z))
        {
            RevertControl();
        }
    }

    internal void TakeControlOf(Controllable c)
    {
        if(c == null)
        {
            Debug.Log("Asked to take control of null");
            return;
        }
        CurrentlyControlled = c;
        inputController = Controllable.ControllerFor(c);
        SetMouseLook(c);
    }

    internal void SetMouseLook(Controllable c)
    { 
        var newMouseLook = c.GetComponent<MouseLook>();
        if (newMouseLook != null)
        {
            if (mlook != null) mlook.enabled = false;
            mlook = newMouseLook;
            mlook.enabled = true;
        }
        transform.SetParent(mlook.head, false);
    }

    internal Controllable RevertControl()
    {
        var oldControl = CurrentlyControlled;
        TakeControlOf(OriginalControlled);
        return oldControl;
    }
}
