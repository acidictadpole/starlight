using Cinemachine;
using UnityEngine;
// Implements mouselook. Horizontal mouse movement rotates the body
// around the y-axis, while vertical mouse movement rotates the head
// around the x-axis.
public class MouseLook : MonoBehaviour
{
    // The speed at which we turn. In other words, mouse sensitivity.
    [SerializeField] float turnSpeed = 90f;

    // How far up the head can tilt, measured in angles from dead-
    // level. Must be higher than headLowerAngleLimit.
    [SerializeField] float headUpperAngleLimit = 85f;

    // How far down the head can tilt, measured in angles from dead-
    // level. Must be lower than headLowerAngleLimit.
    [SerializeField] float headLowerAngleLimit = -80f;

    // Our current rotation from our start, in degrees
    float yaw = 0f;
    float pitch = 0f;

    // Stores the orientations of the head and body when the game
    // started. We'll derive new orientations by combining these
    // with our yaw and pitch.
    Quaternion bodyStartOrientation;
    Quaternion headStartOrientation;

    public Transform head;
    public Transform body;

    // When the game starts, perform initial setup.
    void Awake()
    {
        enabled = false;
        // Cache the orientation of the body and head
        bodyStartOrientation = body.transform.rotation;
        headStartOrientation = head.transform.rotation;

        // Lock and hide the cursor
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Every time physics updates, update our movement. (We do this in
    // FixedUpdate to keep pace with physically simulated objects. If
    // you won't be interacting with physics objects, you can do
    // this in Update instead.)
    void FixedUpdate()
    {

        // Read the current horizontal movement, and scale it based on
        // the amount of time that's elapsed and the movement speed.
        var horizontal = Input.GetAxis("Mouse X")
                              * Time.deltaTime * turnSpeed;

        // Same for vertical
        var vertical = -Input.GetAxis("Mouse Y") 
                              * Time.deltaTime * turnSpeed;

        // Update our yaw and pitch values.
        yaw += horizontal;
        pitch += vertical;

        // Clamp pitch so that we can't look directly down or up.
        pitch =
            Mathf.Clamp(pitch, headLowerAngleLimit, headUpperAngleLimit);

        // Compute a rotation for the body by rotating around the y-axis
        // by the number of yaw degrees, and for the head around the
        // x-axis by the number of pitch degrees.
        var bodyRotation = Quaternion.AngleAxis(yaw, Vector3.up);
        var headRotation = Quaternion.AngleAxis(pitch, Vector3.right);

        // Create new rotations for the body and head by combining them
        // with their start rotations.
        body.localRotation = bodyRotation * bodyStartOrientation;
        head.localRotation = headRotation * headStartOrientation;
    }

}