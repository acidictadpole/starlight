﻿using UnityEngine;
using System.Collections;
using UnityEngine.UIElements;

//TODO: Event driven.
public class MouseManager : MonoBehaviour {

    public GameObject HoveredObject { get; private set; }
    Camera mainCamera;

    // Use this for initialization
    void Awake () {
        mainCamera = GetComponent<Camera>();
    }
    
    // Update is called once per frame
    void Update () {
        Ray ray = mainCamera.ScreenPointToRay( Input.mousePosition );
        RaycastHit hitInfo;

        if( Physics.Raycast( ray, out hitInfo ) ) {
            GameObject hitObject = hitInfo.collider.gameObject;

            HoverObject(hitObject);
        }
        else {
            ClearHover();
        }

        if (Input.GetMouseButtonDown(0))
        {
            Click();
        }
    }
    void Click()
    {
        if (HoveredObject != null)
        {
            var controller = GetComponent<PlayerController>();
            if (controller != null)
            {
                var interactable = HoveredObject.GetComponentInChildren<Interactable>();
                interactable?.Interact(Interactable.InteractionType.CLICK, controller);
            }
        }
    }

    void HoverObject(GameObject obj) {
        if(HoveredObject != null) {
            if(obj == HoveredObject)
                return;
            ClearHover();
        }

        var controller = mainCamera.GetComponentInParent<PlayerController>();
        if (controller != null)
        {
            var interactable = obj.GetComponentInChildren<Interactable>();
            interactable?.Interact(Interactable.InteractionType.HOVER, controller);

        } else
        {
            Debug.LogFormat("Controller was null on {0}", mainCamera.name);
        }
        HoveredObject = obj;
    }

    void ClearHover() {
        if(HoveredObject == null)
            return;


        HoveredObject = null;
    }
}
