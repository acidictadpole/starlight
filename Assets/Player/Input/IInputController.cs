﻿public interface IInputController
{
    public void HandleInput();
}