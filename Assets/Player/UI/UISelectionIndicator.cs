using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UISelectionIndicator : MonoBehaviour
{
    MouseManager mm;
    Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        mm = FindObjectOfType<MouseManager>();
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if(mm.HoveredObject != null && mm.HoveredObject.GetComponent<Interactable>() != null)
        {
            string guiName = mm.HoveredObject.GetComponent<Interactable>().guiName;
            for (int i = 0; i < this.transform.childCount; i++)
            {
                this.transform.GetChild(i).gameObject.SetActive(true);
            }
            Bounds bigBounds = mm.HoveredObject.GetComponentInChildren<Renderer>().bounds;

            RectTransform rect = GetComponent<RectTransform>();
            rect.position = cam.WorldToScreenPoint(bigBounds.center);

            updateSelectorText(guiName);
        } else
        {
            for (int i = 0; i < this.transform.childCount; i++)
            {
                this.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
    }

    private void updateSelectorText(string selectorText)
    {
        var textComponent = GetComponentInChildren<TextMeshProUGUI>();
        textComponent.text = selectorText;
    }
}
