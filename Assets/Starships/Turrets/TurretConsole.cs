using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretConsole : MonoBehaviour
{
    Interactable thisInteraction;
    [SerializeField]
    Controllable[] turrets;
    Controllable currentTurret;
    void Awake()
    {
        thisInteraction = gameObject.GetOrAddComponent<Interactable>();
        thisInteraction.guiName = "Turret Controller";
        thisInteraction.Interaction += OnInteraction;
    }
    
    private void OnInteraction(object sender, Interactable.InteractionEventArgs e)
    {
        if (e.interactionType == Interactable.InteractionType.CLICK)
        {
            if (currentTurret == null) currentTurret = turrets[0];
            e.interactor.TakeControlOf(currentTurret);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
