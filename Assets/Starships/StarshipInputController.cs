using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarshipInputController : IInputController
{
    readonly Rigidbody rb;
    public StarshipInputController(GameObject ship)
    {
        Debug.Log($"Assuming Control! {ship.name}");
        rb = ship.GetOrAddComponent<Rigidbody>();
        rb.useGravity = false;
    }

    public void HandleInput()
    {
        if (rb == null) return;
        if(Input.GetKey(KeyCode.W))
        {
            Vector3 thrust = rb.transform.forward * 1f;
            rb.AddForce(thrust);
        }
        if(Input.GetKey(KeyCode.S))
        {
            Vector3 thrust = rb.transform.forward * -1f;
            rb.AddForce(thrust);
        }
        if(Input.GetKey(KeyCode.R))
        {
            Vector3 thrust = rb.transform.up * 1f;
            rb.AddForce(thrust);
        }
        if(Input.GetKey(KeyCode.F))
        {
            Vector3 thrust = rb.transform.up * -1f;
            rb.AddForce(thrust);
        }
        if(Input.GetKey(KeyCode.A))
        {
            rb.AddTorque(rb.transform.up * -.5f);
        }
        if(Input.GetKey(KeyCode.D))
        {
            rb.AddTorque(rb.transform.up * .5f);
        }
        //STOP THE SHIP!
        if(Input.GetKey(KeyCode.X))
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
        }
    }


}
