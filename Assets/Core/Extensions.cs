﻿using UnityEngine;

static class Extensions
{
    public delegate void Thunk<T>(T v);
    /// <summary>
    /// If the underlying T is present, the Thunk will be applied to it.
    /// </summary>
    /// <typeparam name="T">The nullable type</typeparam>
    /// <param name="t">This nullable type</param>
    /// <param name="f">The thunk/consumer for the type T.</param>
    public static void IfPresent<T>(this T? t, Thunk<T> f) where T : struct
    {
        if (t.HasValue)
        {
            f(t.Value);
        }
    }

    /// <summary>
    /// Will search for a component on a gameobject. 
    /// If it finds one, it will return it.
    /// If it doesn't find one, it will add it and then return it.
    /// </summary>
    /// <typeparam name="T">Component Type</typeparam>
    /// <returns></returns>
    public static T GetOrAddComponent<T>(this GameObject gameObject) where T : Component
    {
        //This can't be a `switch` statement apparently. 
        // Unity overloads `==` so it needs to use that.
        var component = gameObject.GetComponent<T>();
        if (component == null) component = gameObject.AddComponent<T>();
        return component as T;
    }
}
